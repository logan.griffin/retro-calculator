Retro Calculator - Created 27/08/16, by Logan Griffin

This project was created while doing an iOS course apart of my learning. 
This app was made alongside the iOS course "iOS 9 and Swift 2: From Beginner to
Paid Professional" created by Mark Price.

All graphics from this application were supplied from Mark Price and downloaded
for the udemy website. Retro Calculator applicaton was created for learning
purposes, this application is not intended for public use.