//
//  ViewController.swift
//  retro-calculator
//
//  Created by Logan Griffin on 27/08/16.
//  Copyright © 2016 Deepsouth Technology. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    // enum is a data type (Int, Double, String etc.)
    enum Operation: String {
        case Divide = "/"
        case Multiply = "*"
        case Subtract =  "-"
        case Add = "+"
        case Empty = "Empty"
    }

    @IBOutlet weak var outputLabel: UILabel!
    @IBOutlet weak var clearButton: UIButton!
    
    var btnSound: AVAudioPlayer!
    
    var runningNumber = ""
    var leftValString = ""
    var rightValString = ""
    var currentOperation: Operation = Operation.Empty
    var result = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let path = NSBundle.mainBundle().pathForResource("btn", ofType: "wav")
        let soundURL = NSURL(fileURLWithPath: path!)
        
        do {
            try btnSound = AVAudioPlayer(contentsOfURL: soundURL)
            btnSound.prepareToPlay()
        } catch let err as NSError {
            print(err.debugDescription)
        }
    }

    @IBAction func numberPressed(btn: UIButton!) {
        playSound()
        
        runningNumber += "\(btn.tag)"
        outputLabel.text = runningNumber
    }
    
    @IBAction func dividePressed(sender: AnyObject) {
        processOperation(Operation.Divide)
    }
    
    @IBAction func multiplicationPressed(sender: AnyObject) {
        processOperation(Operation.Multiply)
    }
    
    @IBAction func subtractionPressed(sender: AnyObject) {
        processOperation(Operation.Subtract)
    }
    
    @IBAction func additionPressed(sender: AnyObject) {
        processOperation(Operation.Add)
    }
    
    
    @IBAction func equalPressed(sender: AnyObject) {
        processOperation(currentOperation)
    }
    
    
    @IBAction func clearButtonPressed(sender: AnyObject) {
        runningNumber = ""
        leftValString = ""
        rightValString = ""
        currentOperation = Operation.Empty
        result = ""
        outputLabel.text = ""
        playSound()
    }
    
    
    func processOperation(op: Operation) {
        playSound()
    
        if currentOperation != Operation.Empty {
            
            // run some math
            if runningNumber != "" {
            
                rightValString = runningNumber
                runningNumber = ""
            
                if currentOperation == Operation.Multiply{
                    result = "\(Double(leftValString)! * Double(rightValString)!)"
                } else if currentOperation == Operation.Divide{
                    result = "\(Double(leftValString)! / Double(rightValString)!)"
                } else if currentOperation == Operation.Add{
                    result = "\(Double(leftValString)! + Double(rightValString)!)"
                } else if currentOperation == Operation.Subtract{
                    result = "\(Double(leftValString)! - Double(rightValString)!)"
                }
            
                leftValString = result
                outputLabel.text = result
            }
            
            currentOperation = op
            
        } else {
        // first time an operator has been pressed
            leftValString = runningNumber
            runningNumber = ""
            currentOperation = op
        }
    }
    
    func playSound() {
        if btnSound.playing {
            btnSound.stop()
        } else {
            btnSound.play()
        }
    }
}

